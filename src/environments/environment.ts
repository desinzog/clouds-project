// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBV6BxA_xdKzSZimJJYzfJcNORJ1Q4i--E",
    authDomain: "cloudvid19.firebaseapp.com",
    projectId: "cloudvid19",
    storageBucket: "cloudvid19.appspot.com",
    messagingSenderId: "634492262832",
    appId: "1:634492262832:web:98f860498a2b70282a5e75",
    measurementId: "G-J441KSEH8E"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
