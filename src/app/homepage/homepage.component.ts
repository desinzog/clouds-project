import { Component, OnInit } from '@angular/core';
import { ByCountry } from '../covid_by_country.model';
import {CovidService} from '../covid.service';
import {Worldwide} from '../covid-worlwide.model';
import {Chart, ChartDataSets, ChartOptions, ChartType} from 'chart.js'
import { Color } from 'chartjs-plugin-datalabels/types/options';
import { Label } from 'ng2-charts';
import { NewsService} from '../news.service';
import {News} from '../news.model'
import {User} from '../user.model';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  public countries : ByCountry[];
  public worldwide : Worldwide;
  public pieChartLabels: Label[];
  public user : User;
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public news : News[];

  public pieChartData: ChartDataSets[];

  public pieChartOptions: ChartOptions = {
     responsive : true
    }

    public barChartLabels: Label[];

    public barChartType: ChartType = 'bar';
    public barChartLegend = true;

    public barChartData: ChartDataSets[];

    public barChartOptions: ChartOptions = {
        responsive: true,
    }


    public lineChartLabels: Label[];

    public lineChartType: ChartType = 'line';
    public lineChartLegend = true;

    public lineChartColor:any = [
      {
        backgroundColor: 'rgba(236, 248, 127,0.7)'
      },
      {
        backgroundColor: 'rgba(255, 235, 205,0.5)' 
      },
      {
        backgroundColor: 'rgba(255, 150, 54,1)'
      }
    ]

    public lineChartData: ChartDataSets[];

    public lineChartOptions: ChartOptions = {
        responsive: true,
    }

  constructor(public covidService : CovidService,public newsService: NewsService) { 
    this.countries = []
    this.worldwide= new Worldwide()
    this.pieChartLabels = []
    this.pieChartData = []
    this.barChartData=[]
    this.barChartLabels=[]
    this.lineChartLabels=[]
    this.lineChartData=[]
    this.news=[];
    this.user = null!;
    
  }

 

  ngOnInit() : void {
    this.user = this.newsService.getUser();
    this.countries = this.covidService.getByCountry();
    this.worldwide=this.covidService.getWorldSummary();
    this.pieChartLabels = ['Active Cases', 'Dead Cases', 'Recovered Cases'];
    let colors: Color[] = ["#ffe945A1" ,"#ff4545A1", "#45a8ffA1"];
    let data_array=this.covidService.getPieData();

    this.pieChartData = [{data: data_array, backgroundColor: colors}];
    this.covidService.getBarData().subscribe((data) => {
      
     
  
      let dates_array_initial = Object.keys(data["cases"])
      let deaths_array_initial : number[] = Object.values(data.deaths)
      let cases_array_initial : number[] = Object.values(data.cases)
      let recovered_array_initial : number[] = Object.values(data.recovered)


      let deaths_array : number[] = [];
      let cases_array : number[] = [];
      let recovered_array : number[] = [];
     for (var i = 0; i < dates_array_initial.length-2; i++){
      this.barChartLabels.push(dates_array_initial[i+1]);
      deaths_array.push(deaths_array_initial[i+1]-deaths_array_initial[i]);
      cases_array.push(cases_array_initial[i+1]-cases_array_initial[i]);
      recovered_array.push(recovered_array_initial[i+1]-recovered_array_initial[i]);
    }
    this.barChartData = [
      {data: deaths_array, label: 'Daily Deaths', backgroundColor: "#ff4545A1"},
      {data: cases_array, label: 'Daily New Cases', backgroundColor: "#ffe945A1"},
      {data: recovered_array, label: 'Daily Recovered', backgroundColor: "#45a8ffA1"}
    ];
  
  
  
    });
    this.covidService.getLineData().subscribe((data_values)=>{
      this.lineChartLabels = Object.keys(data_values["cases"])
      let deaths_array : number[] = Object.values(data_values.deaths)
      let cases_array : number[] = Object.values(data_values.cases)
      let recovered_array : number[] = Object.values(data_values.recovered)

      this.lineChartData = [
        {data: deaths_array, label: 'Daily Deaths', borderColor: "#000000", backgroundColor: "#ff4545A1"},
        {data: cases_array, label: 'Daily New Cases', borderColor: "#ffe945A1", backgroundColor: "#ffe945A1", pointBackgroundColor: "ffe945A1"},
        {data: recovered_array, label: 'Daily Recovered', borderColor: "#4248f5", backgroundColor: "#45a8ffA1", pointBackgroundColor: "45a8ffA1"}
      ];
    })

    this.newsService.getNews().subscribe((news)=>{
      let country_news = []
      for(let i=0; i<news.length; i++){
        var new_i = news[i] as News;
        if(new_i.country == "Worldwide"){
          country_news.push(new_i);
        }
      }
      this.news = country_news;
    });

    

}
}
