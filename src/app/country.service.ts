import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ByCountry } from './covid_by_country.model';
import { CovidService } from './covid.service';

@Injectable({
  providedIn: 'root'
})
export class CountryService {
  country: ByCountry = new ByCountry();

  constructor(private httpClient: HttpClient,
              private firestore:AngularFirestore,
              private covidService:CovidService) { }

  public getCountryAPICall(slug:string){ 
    let string = "https://api.covid19api.com/total/country/" + slug;
    return this.httpClient.get<any>(string);
  }
  private updateCountryData(country: ByCountry){
   
  }
}