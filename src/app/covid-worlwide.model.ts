export class Worldwide{
    total_cases : number;
    new_cases  : number;
    active_cases  :number;
    total_recovered:number;
    new_recovered:number;
    recovery_rate:number;
    total_deaths:number;
    new_deaths:number;
    mortality_rate:number;
    
    constructor(){
    this.total_cases =0;
    this.new_cases  =0;
    this.active_cases  = 0;
    this.total_recovered = 0;
    this.new_recovered =0;
    this.recovery_rate=0;
    this.total_deaths = 0;
    this.new_deaths =0 ;
    this.mortality_rate = 0;
    }
}