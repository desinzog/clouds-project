import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import { AngularFireAuth} from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from './user.model';
import { Router } from '@angular/router';
import { News } from './news.model';
@Injectable({
  providedIn: 'root'
})
export class NewsService {

  private user!: User;

  constructor(private afAuth: AngularFireAuth, 
    private router: Router, private firestore : AngularFirestore) {
      
    }

  async signInWithGoogle() {
   const credientals : any= await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()); 
   this.user = {
      uid: credientals.user.uid,
      displayName: credientals.user.displayName,
      email: credientals.user.email
    };
    localStorage.setItem("user", JSON.stringify(this.user));
    this.updateUserData();
    this.router.navigate(["homepage"]);
  }

  private updateUserData(){
    this.firestore.collection("users").doc(this.user.uid).set({
      uid: this.user.uid,
      displayName: this.user.displayName,
      email: this.user.email
    }, { merge: true});
  }

  getUser(){
    if(this.user == null && this.userSignedIn()){
      this.user = JSON.parse(localStorage.getItem("user"));
    }
    return this.user;
  }

  userSignedIn(): boolean{
    return JSON.parse(localStorage.getItem("user")) != null;
  }

  signOut(){
    this.afAuth.signOut();
    localStorage.removeItem("user");
    this.user = { 
      uid:"",
      displayName :"",
      email:"",
    };
    this.router.navigate(["signin"]);
  }
  addNews(news: News){
    this.firestore.collection("news").add(news);
  }

  getNews(){
    return this.firestore
    .collection("news", ref => ref.orderBy('date', 'desc'))
    .valueChanges();
  }
}
