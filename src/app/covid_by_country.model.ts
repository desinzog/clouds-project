export class ByCountry{

    country : string;
    new_cases  : number;
    total_cases  :number;
    new_reco : number;
    total_reco  :number;
    new_death : number;
    total_deaths  :number;
    
    constructor(){
        this.country="";
        this.new_cases=0;
        this.total_cases=0;
        this.new_reco=0;
        this.total_reco=0;
        this.new_death=0;
        this.total_deaths=0;
    }
}