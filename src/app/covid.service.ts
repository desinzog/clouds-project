import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import {ByCountry} from './covid_by_country.model';
import { range } from 'rxjs';
import { LeadingComment } from '@angular/compiler';
import {Worldwide} from './covid-worlwide.model'

import {Chart, ChartDataSets, ChartOptions, ChartType} from 'chart.js'
import { Color } from 'chartjs-plugin-datalabels/types/options';
import { Label } from 'ng2-charts';
@Injectable({
  providedIn: 'root'
})
export class CovidService {

  private countries : ByCountry[] ;
  
  private worldwide : Worldwide;
  private data_array: number[][];
  private barChartLabels: Label[];
  private barChartData:ChartDataSets[];

  constructor(private http: HttpClient) { 
    this.countries = [];
    this.data_array=[[],[],[]];
    this.worldwide = new Worldwide();
    this.barChartLabels=[];
    this.barChartData=[];
  }
  


  public getByCountry(){
    this.http.get<any>("https://api.covid19api.com/summary").subscribe((data) => {

    for(let i =0; i < data.Countries.length; i++){
      let countryData = new ByCountry();
      countryData.country=data.Countries[i].Country;
      countryData.new_cases=data.Countries[i].NewConfirmed;
      countryData.total_cases=data.Countries[i].TotalConfirmed;
      countryData.new_reco=data.Countries[i].NewRecovered;
      countryData.total_reco=data.Countries[i].TotalRecovered;
      countryData.new_death=data.Countries[i].NewDeaths;
      countryData.total_deaths=data.Countries[i].TotalDeaths;
      this.countries.push(countryData);
    }
    });

    return this.countries;
    
  }

  public getHttpSummary(){
    
    return this.http.get<any>("https://api.covid19api.com/summary");
  }

  public getWorldSummary(){
    this.http.get<any>("https://api.covid19api.com/summary").subscribe((data) => {
        this.worldwide.total_cases=data.Global.TotalConfirmed;
        this.worldwide.new_cases=data.Global.NewConfirmed;
        this.worldwide.total_recovered=data.Global.TotalRecovered;
        this.worldwide.new_recovered=data.Global.NewRecovered;
        this.worldwide.recovery_rate= Math.round((this.worldwide.total_recovered/this.worldwide.total_cases)*100)
        this.worldwide.new_deaths=data.global.NewDeaths;
        this.worldwide.total_deaths=data.Global.TotalDeaths;
        this.worldwide.mortality_rate=Math.round((this.worldwide.total_deaths/this.worldwide.total_cases)*100);

        this.worldwide.active_cases=data.Global.TotalConfirmed - data.Global.TotalRecovered;

      });
      return this.worldwide;
  }
     
  public getPieData(){
    
    this.http.get<any>("https://corona.lmao.ninja/v2/historical/all?lastdays=1").subscribe((data) => {
      let cases : number[] = Object.values(data.cases);
      let deaths: number[] = Object.values(data.deaths);
      let recovered : number[] = Object.values(data.recovered);
      this.data_array[0]=cases;
      this.data_array[1]=deaths;
      this.data_array[2]=recovered;
      

      });
    return this.data_array;
  }

  public getBarData(){
    return this.http.get<any>("https://corona.lmao.ninja/v2/historical/all?lastdays=8")
    } 


  public getLineData(){
    return this.http.get<any>("https://corona.lmao.ninja/v2/historical/all?lastdays=all");

  }

  public chartCountry(country: string){
    return this.http.get<any>("https://api.covid19api.com/total/dayone/country/"+country);
  }

  }

