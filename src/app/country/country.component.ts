import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CovidService} from '../covid.service'
import{Worldwide} from '../covid-worlwide.model'
import { ByCountry } from '../covid_by_country.model';
import {Chart, ChartDataSets, ChartOptions, ChartType} from 'chart.js'
import { Color } from 'chartjs-plugin-datalabels/types/options';
import { Label } from 'ng2-charts';
import { NewsService} from '../news.service';
import {News} from '../news.model';
@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {

     
  public country_name : string ;

  public news: News[];
  public country_summary: Worldwide;
  public countries : ByCountry[];

  public pieChartLabels: Label[] = ['Active Cases', 'Dead Cases', 'Recovered Cases'];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartData: ChartDataSets[] ;
  public pieChartOptions: ChartOptions = {
      responsive: true,
  }

  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartData: ChartDataSets[] = [];
  public barChartOptions: ChartOptions = {
    responsive: true,
  }

  public lineChartLabels: Label[] = [];
  public lineChartType: ChartType = 'line';
  public lineChartLegend = true;
  public lineChartData: ChartDataSets[] = [];
  public lineChartOptions: ChartOptions = {
    responsive: true,
  }

  constructor(private router: Router, private covidService: CovidService, public newsService: NewsService) {
    this.country_summary=new Worldwide();
    this.country_name="";
    this.countries=[];
    this.pieChartData=[];
    this.news= [];
   }

  ngOnInit(): void {
    this.country_name=this.getName();
    this.countries=this.covidService.getByCountry()
    this.covidService.getHttpSummary().subscribe((data)=>{
      let country_data = this.getCountryData(data.Countries, this.country_name)

      this.country_summary.new_cases = country_data.NewConfirmed;
      this.country_summary.total_cases = country_data.TotalConfirmed;
      this.country_summary.new_deaths = country_data.NewDeaths;
      this.country_summary.total_deaths = country_data.TotalDeaths;
      this.country_summary.new_recovered = country_data.NewRecovered;
      this.country_summary.total_recovered = country_data.TotalRecovered;
      this.country_summary.active_cases = (country_data.TotalConfirmed - country_data.TotalRecovered);
      this.country_summary.recovery_rate = Math.round((country_data.TotalRecovered / country_data.TotalConfirmed)*10000)/100;
      this.country_summary.mortality_rate = Math.round((country_data.TotalDeaths / country_data.TotalConfirmed)*10000)/100; 
    });
    
    let pieChartColors: string[] = ["#f0cf65" ,"#d7816a", "#93b5c6"];

    this.covidService.getHttpSummary().subscribe((data)=>{
      let country_data = this.getCountryData(data.Countries, this.country_name);

      let cases : number = country_data.TotalConfirmed ;
      let deaths: number = country_data.TotalDeaths ;
      let recovered : number = country_data.TotalRecovered ;
      let data_array: number[] = [cases, deaths, recovered];

      this.pieChartData = [{data: data_array, backgroundColor: pieChartColors}];
    });

    this.covidService.chartCountry(this.country_name).subscribe((data)=>{
      let deaths : number[] = [];
      let cases : number[] = [];
      let recovered : number[] = [];
      let N : number = data.length;

      for (let i = 1; i<8; i++){
        let date_split = (data[N-i].Date).split("T");
        let date = date_split[0];
        this.barChartLabels.unshift(date); 

        deaths.unshift(data[N-i].Deaths - data[N-i-1].Deaths);
        cases.unshift(data[N-i].Confirmed - data[N-i-1].Confirmed);
        recovered.unshift(data[N-i].Recovered - data[N-i-1].Recovered);
      }

      this.barChartData = [
        {data: deaths, label: 'Daily Deaths', backgroundColor: "#d7816a"},
        {data: cases, label: 'Daily New Cases', backgroundColor: "#f0cf65"},
        {data: recovered, label: 'Daily Recovered', backgroundColor: "#93b5c6"}
      ];
    });

    this.covidService.chartCountry(this.country_name).subscribe((data)=>{
      let deaths : number[] = [];
      let cases : number[] = [];
      let recovered : number[] = [];

      for (let i = 0; i < data.length; i+=4){

        let date_split = (data[i].Date).split("T");
        let date = date_split[0];
        this.lineChartLabels.push(date);

        deaths.push(data[i].Deaths);
        cases.push(data[i].Confirmed);
        recovered.push(data[i].Recovered);
      }

      this.lineChartData = [
        {data: deaths, label: 'Daily Deaths', backgroundColor: "#d7816a"},
        {data: cases, label: 'Daily New Cases', backgroundColor: "#f0cf65"},
        {data: recovered, label: 'Daily Recovered', backgroundColor: "#93b5c6"}
      ];
    });
    this.newsService.getNews().subscribe((news)=>{
      let country_news = []
      for(let i=0; i<news.length; i++){
        var new_i = news[i] as News;
        if(new_i.country == "Worldwide"){
          country_news.push(new_i);
        }
      }
      this.news = country_news;
    });

  }

  
    public getName(){
      let url = this.router.url;
      url = decodeURI(url);
      return url.split("/")[2];
  
    }
    public getCountryData(jsonObject : any, name : string){
      return jsonObject.find((element: { Country: string; }) => element.Country === name);
    }
  }


