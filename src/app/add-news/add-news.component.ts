import { Component, OnInit } from '@angular/core';
import {News} from "../news.model";
import { CovidService } from '../covid.service';
import { User } from '../user.model';
import { ResourceLoader } from '@angular/compiler';
import { NewsService } from '../news.service';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {
  user: User;

  date: any;
  country: String;
  news: String;
  constructor(public covidService: CovidService, public newsService: NewsService) { }

  ngOnInit(): void {
    this.user = this.newsService.getUser();
  }
  addNews(){
    const news: News = {
      date: new Date(this.date),
      country: this.country,
      news: this.news,
      uid: this.user.uid,
      name: this.user.displayName,
      email: this.user.email
    };
    this.newsService.addNews(news);
    this.date = undefined;
    this.news = undefined!;
    this.country = undefined!;
  }
}
