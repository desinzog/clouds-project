import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountryComponent } from './country/country.component';
import { HomepageComponent } from './homepage/homepage.component';
import { SigninComponent } from './signin/signin.component';
import { AddNewsComponent } from './add-news/add-news.component';
import { AuthGuard } from './auth.guard';
import { SecurePagesGuard } from './secure-pages.guard';
const routes: Routes = [
  {path: "signin", component:SigninComponent},
  {path :"homepage",component: HomepageComponent},
  {path: "country/:countryName", component: CountryComponent},
  { path: "signin", component: SigninComponent, 
  canActivate: [SecurePagesGuard]},
  { path: "add-news", component: AddNewsComponent,
  canActivate: [AuthGuard]},
  {path:"", pathMatch:"full",redirectTo: "homepage"},
  {path:"**", redirectTo:"homepage"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
